Ricci Tensor Module
===================

This module contains the class for obtaining Ricci Tensor
related to a Metric belonging to any arbitrary space-time symbolically:

.. automodule:: einsteinpy.symbolic.ricci
    :members:
